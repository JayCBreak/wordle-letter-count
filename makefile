count: count.o
	clang -std=c99 -Wall -pedantic count.o -o count

count.o: count.c
	clang -std=c99 -Wall -pedantic -c count.c 

clean:
	rm -rf *.o count