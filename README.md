# wordle-letter-count




## Description
This project reads in the provided file "wordle-answers-alphabetical.txt" provided by tyleryzhu: https://github.com/tyleryzhu/wordle-bot/blob/main/wordle-answers-alphabetical.txt  
It then counts the various characters in each of the 5 possible positions and outputs the data found by it all.

## Installation
Compilation is easy! Simply ensure you have clang installed on your system and then run:  
```
make
```

To remove compiled files simply run:  
```
make clean
```

## Usage
Running the program is simple! Ensure you have the count executable in the same folder as "wordle-answers-alphabetical.txt" and then run ./count in the terminal.

## Support
This project supports systems with the clang compiler out of the box. 

## Roadmap
- [x] Reading Files
- [x] Added Dev outputs
- [x] Counting Chars
- [x] Counting Totals of Positions
- [x] Calculating Percentages
- [ ] User input files
- [ ] User input word length
- [ ] Support for non-ASCII characters

## Authors and acknowledgment
Jacob - Programmer  
Tyleryzhu - Wordle list provider  

## License
This Open Source project is licensed under the GNU General Public License v3.0

## Project status
Project is complete for calculating the "wordle-answers-alphabetical.txt" file.  
60% complete
