#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILENAME "wordle-answers-alphabetical.txt"
#define ALPHA_LEN 26
#define WORD_LEN 5

int main (int argc, char **argv);
void count(unsigned int alphabet[WORD_LEN][ALPHA_LEN], unsigned int total[WORD_LEN]);
void print(unsigned int alphabet[WORD_LEN][ALPHA_LEN], unsigned int total[WORD_LEN]);
void clearAlphabet(unsigned int alphabet[WORD_LEN][ALPHA_LEN]);
