#include "count.h"

//#define dev

int main (int argc, char **argv) {
    printf("Calculating occurance of various letters:\n");
    unsigned int alphabet[WORD_LEN][ALPHA_LEN];
    clearAlphabet(alphabet);
    unsigned int total[WORD_LEN];
    for(int i=0; i<WORD_LEN; i++) {
        total[i] = 0;
    }
    count(alphabet, total);
    print(alphabet, total);
}

void count(unsigned int alphabet[WORD_LEN][ALPHA_LEN], unsigned int total[WORD_LEN]) {
    int i = 0;
    int j = 0;
    char word[10];
    FILE *fp;
    fp = fopen(FILENAME, "r");
#ifdef dev
    FILE *log;
    log = fopen("log.txt", "w");
#endif
    if(!fp) {
        fprintf(stderr, "[ERROR] - Missing file: %s\n", FILENAME);
        exit(-1);
    }
    while(!feof(fp)) {
        fscanf(fp, "%s", word);
        for(i=0; i<WORD_LEN; i++) {
            if(word[i] >= 'A' && word[i] <= 'Z') {
                alphabet[i][word[i] - 'A']++;
            }
            if(word[i] >= 'a' && word[i] <= 'z') {
                alphabet[i][word[i] - 'a']++;
            }
        }
#ifdef dev
fprintf(log, "Word: %s\n", word);
fprintf(log, "The total raw characters are:\n");
int k = 0;
int l = 0;
for(k=0; k<WORD_LEN; k++) {
    fprintf(log, "Character %d: \n", k);
    for(l=0; l<ALPHA_LEN; l++) {
        fprintf(log, "%c: %d | ", 'A'+l, alphabet[k][l]);
    }
    fprintf(log, "\n");
}
#endif
    }
    for(i=0; i<WORD_LEN; i++) {
        for(j=0; j<ALPHA_LEN; j++) {
            total[i] += alphabet[i][j];
        }
#ifdef dev
printf("Char: %d total: %u\n", i, total[i]);
#endif
    }
    fclose(fp);
#ifdef dev
fclose(log);
#endif
}

void print(unsigned int alphabet[WORD_LEN][ALPHA_LEN], unsigned int total[WORD_LEN]) {
    int i = 0;
    int j = 0;
    printf("The total raw characters are:\n");
    for(i=0; i<WORD_LEN; i++) {
        printf("Character %d: \n", i);
        for(j=0; j<ALPHA_LEN; j++) {
            printf("%c: %d | ", 'A'+j, alphabet[i][j]);
        }
        printf("\n");
    }
    printf("----------------------------------------------------------------- \n");
    printf("The total character %%'s are:\n");
    
    for(i=0; i<WORD_LEN; i++) {
        printf("Character %d: \n", i);
        for(j=0; j<ALPHA_LEN; j++) {
            printf("%c: %3.1f%% | ", 'A'+j, 100*(double)alphabet[i][j]/total[i]);
        }
        printf("\n");
    }
}

void clearAlphabet(unsigned int alphabet[WORD_LEN][ALPHA_LEN]) {
    int i = 0;
    int j = 0;
    for(i=0; i<WORD_LEN; i++) {
        for(j=0; j<ALPHA_LEN; j++) {
            alphabet[i][j] = 0;
        }
    }
}
